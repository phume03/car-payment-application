#!/usr/bin/env python
# @user: Phumelela Mdluli
# @homework: 01 Program #2
# @date: 23/July/2018
# @description: upgrading car payment script to a widget/app, exclusively using Fredrik Lundh techniques.
# @python version: Python 2.7
# @notes: 
#   Tkinter widget names cannot start in capital letters
#   wait_window()
#   grab_set() events are directed to window with focus
#   focus_set() sets active window

from Tkinter import *
import sys
import tkMessageBox as popup

class Car_Payment_App():
  options = {
    'title':'Car Payment Application',
    'appTitle':'Car Installment Calculator',
    'about':'About CPA',
    'quitLabel':'Done',
    'exitButton':'exitButton',
    'calcLabel':'Calculate',
    'calcButton':'calculateButton',
    'height':600,
    'width':600,
    'cp_units':'E',
    'cp_var_text':'Current Price:',
    'dp_units':'E',
    'dp_var_text':'Down Payment:',
    'ti_units':'E',
    'ti_var_text':'Trade In:',
    'tiy_units':'years',
    'tiy_var_text':'Time In Years:',
    'yi_units':'%',
    'yi_var_text':'Yearly Interest:'
  }

  car_payment_values = {
    'dp_var':0,
    'ti_var':0,
    'yi_var':0,
    'tiy_var':0,
    'cp_var':0,
    'mp_var':0
  }

  def __init__(self, master):
    master.resizable(width=FALSE, height=FALSE)
    #master.minsize(height=self.options['height'], width=self.options['width'])
    master.title(self.options['title'])
    #master.bind(sequence="<Button-1>", func=self.mouse_right_click)
    master.protocol(name='WM_DELETE_WINDOW', func=self.x_window_quit)
    
    self.add_menu(master)

    frame = Frame(master, {'relief':'flat', 'bg':'black'})
    frame.pack({'fill':'both', 'expand':'true'})
    frame.grid({'row':0, 'column':0, 'ipadx':10, 'ipady':10})
    
    self.add_widgets(frame)
    return

  def add_menu(self, master):
    menu = Menu(master)
    menu.add_command(label=self.options['about'], command=self.about)   
    master.config(menu=menu)
    return

  def about(self):
    aboutmessage = 'Created by: Phumelela Mdluli\nEmail: pumie03@gmail.com\nPhone: +(268) 7667 3231\n\nThe {APPNAME} is an advancement of the simple downpayment script created in CSC112 at Lake Forest College. It introduces basic GUI concepts, paying some respect to UI/UX principles.'.format(APPNAME=self.options['title'])
    popup.showinfo(self.options['about'],message=aboutmessage)
    return

  def add_widgets(self, master):
    master.grid_columnconfigure(index=0, uniform=1)
    master.grid_columnconfigure(index=1, uniform=1) 
    master.grid_columnconfigure(index=3, uniform=1)
    
    titleFont = {'family':'Times New Roman', 'size':18, 'weight':'BOLD'}
    mainlabel = Label(master, text=self.options['appTitle'], font=titleFont, background='black', foreground='white')
    mainlabel.pack({'fill':'both', 'expand':'true'})
    mainlabel.grid({'column':0, 'row':0, 'columnspan':4, 'sticky':'e w', 'ipadx':10, 'ipady':10})
    
    self.cp_var = DoubleVar()
    self.dp_var = DoubleVar()
    self.ti_var = DoubleVar()
    self.tiy_var = IntVar()
    self.yi_var = DoubleVar()
    self.mp_var = StringVar()

    self.cp_var.set('0.00')
    self.dp_var.set('0.00')
    self.ti_var.set('0.00')
    self.tiy_var.set('0')
    self.yi_var.set('000.01')
    self.mp_var.set('...')

    # Fonts
    labelFont = {'family':'Times New Roman', 'size':12}
    unitsFont = {'family':'Times New Roman', 'size':12}
    outputFont = {'family':'Times New Roman', 'size':12}

    # Labels CP, DP, TI, TIY, YI
    Label(master, text=self.options['cp_var_text'], font=labelFont, background='black', foreground='red').grid({'column':0, 'row':1, 'columnspan':3, 'sticky':'e w', 'padx':2, 'pady':2})
    Label(master, text=self.options['cp_units'], font=unitsFont, background='black', foreground='white').grid({'column':0, 'row':2, 'sticky':'e', 'padx':2, 'pady':2})
    self.cpEntry = Entry(master, bg='gray', width=13, textvariable=self.cp_var)
    self.cpEntry.grid({'column':1, 'row':2, 'ipadx':5, 'ipady':5, 'padx':2, 'pady':2})
    Label(master, text=self.options['dp_var_text'], font=labelFont, background='black', foreground='red').grid({'column':0, 'row':3, 'columnspan':3, 'sticky':'e w', 'padx':2, 'pady':2})
    Label(master, text=self.options['dp_units'], font=unitsFont, background='black', foreground='white').grid({'column':0,'row':4, 'sticky':'e', 'padx':2, 'pady':2})
    self.dpEntry = Entry(master, textvariable=self.dp_var, bg='gray', width=13)
    self.dpEntry.grid({'column':1, 'row':4, 'ipadx':5, 'ipady':5, 'padx':2, 'pady':2})
    Label(master, text=self.options['ti_var_text'], font=labelFont, background='black', foreground='red').grid({'column':0, 'row':5, 'columnspan':3, 'sticky':'e w', 'padx':2, 'pady':2})
    Label(master, text=self.options['ti_units'], font=unitsFont, background='black', foreground='white').grid({'column':0, 'row':6, 'sticky':'e', 'padx':2, 'pady':2})
    self.tiEntry = Entry(master, textvariable=self.ti_var, bg='gray', width=13)
    self.tiEntry.grid({'column':1, 'row':6, 'ipadx':5, 'ipady':5, 'padx':2, 'pady':2})
    Label(master, text=self.options['tiy_var_text'], font=labelFont, background='black', foreground='red').grid({'column':0, 'row':7, 'columnspan':3, 'sticky':'e w', 'padx':2, 'pady':2})
    Label(master, text=self.options['tiy_units'], font=unitsFont, background='black', foreground='white').grid({'column':2, 'row':8, 'sticky':'w', 'padx':2, 'pady':2})
    self.tiyEntry = Entry(master, textvariable=self.tiy_var, bg='gray', width=3)
    self.tiyEntry.grid({'column':1, 'row':8, 'sticky':'e', 'ipadx':5, 'ipady':5, 'padx':2, 'pady':2})
    Label(master, text=self.options['yi_var_text'], font=labelFont, background='black', foreground='red').grid({'column':0, 'row':9, 'columnspan':3, 'sticky':'e w', 'padx':2, 'pady':2})
    Label(master, text=self.options['yi_units'], font=unitsFont, background='black', foreground='white').grid({'column':2, 'row':10, 'sticky':'w', 'padx':2, 'pady':2})
    self.yiEntry = Entry(master, textvariable=self.yi_var, bg='gray', width=6)
    self.yiEntry.grid({'column':1, 'row':10, 'sticky':'e', 'ipadx':5, 'ipady':5, 'padx':2, 'pady':2})

    # Output
    self.mpDisplay = Message(master, bg='black', fg='white', font=outputFont, textvariable=self.mp_var)
    self.mpDisplay.grid({'column':0, 'row':12, 'columnspan':4, 'rowspan':4, 'sticky':'n e s w', 'padx':2, 'pady':2, 'ipadx':5, 'ipady':5})

    # Controls
    Button(master, text=self.options['quitLabel'], fg='red', command=self.button_quit, name=self.options['exitButton']).grid({'column':2, 'row':16, 'sticky':'w', 'padx':2, 'pady':2, 'ipadx':5, 'ipady':5})
    Button(master, text=self.options['calcLabel'], bg='blue', fg='white', command=self.car_payment_calculation, name=self.options['calcButton']).grid({'column':1, 'row':16,'sticky':'e', 'padx':2, 'pady':2, 'ipadx':5, 'ipady':5})
    return

  def mouse_right_click(self, event):
    print "clicked at", event.x, event.y,"by",event.num
    return

  def x_window_quit(self):
    print "Exiting via the X11 window."
    quit()
    return

  def button_quit(self):
    print 'Exiting via done-button.'
    quit()
    return

  def car_payment_calculation(self):
    #the current price
    CurrentPrice = self.cp_var.get()
    #the downpayment
    DownPayment = self.dp_var.get()
    #value of trade-in
    TradeIn = self.ti_var.get()
    #years to repay
    TimeInYears = self.tiy_var.get()
    #yearly interest
    YearlyInterest = self.yi_var.get()

    #Calculations:
    YearlyInterestPercent = (YearlyInterest/100.0)
    MonthlyInterest = (YearlyInterestPercent/12.0)
    NumberOfPayments = (TimeInYears*12.0)

    Numerator = ((1 + MonthlyInterest)**NumberOfPayments)
    Denominator = (Numerator - 1)
    CompoundingFactor = 0

    try:
      CompoundingFactor = (Numerator / Denominator)
    except:
      errormessage=str(sys.exc_info()[1])
      popup.showerror(title='An Error Occured', message=errormessage)
    finally:
      AmountOwed = (CurrentPrice - (DownPayment+TradeIn))
      AmountOwed = float(AmountOwed)
      MonthlyPayment = (MonthlyInterest*AmountOwed*CompoundingFactor)
    
    self.car_payment_values['dp_var'] = DownPayment
    self.car_payment_values['ti_var'] = TradeIn
    self.car_payment_values['yi_var'] = YearlyInterest
    self.car_payment_values['tiy_var'] = TimeInYears
    self.car_payment_values['cp_var'] = CurrentPrice
    self.car_payment_values['mp_var'] = MonthlyPayment

    self.display_output()
    return

  def display_output(self):
    final_string = 'With a down payment of E{DOWNP:.2f}, a trade-in value of E{TRADEIN:.2f}, an annual interest rate of {INTRATE:.2f}% and a {TIMEYEARS}-year payment plan, your new E{CURPRICE:.2f} car will cost you only E{MONPAY:.2f} per month.'.format(DOWNP=self.car_payment_values['dp_var'], TRADEIN=self.car_payment_values['ti_var'], INTRATE=self.car_payment_values['yi_var'], TIMEYEARS=self.car_payment_values['tiy_var'], CURPRICE=self.car_payment_values['cp_var'], MONPAY=self.car_payment_values['mp_var'])
    final_string = str(final_string)
    self.mp_var.set(final_string)
    return

root = Tk()
Car_Payment_App(root)
root.mainloop() 
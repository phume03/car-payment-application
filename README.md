# Car Payment Application

This is a basic program/script to simulate a Car Payment financial calculator.  

Initially, I used the jython environment, that is **JES 4.3**. But, I migrated the project to **python 2.7**. 
* * *
## Requirements
This project makes use of **tkinter** to produce a point and click user interface (UI). To install tkinter, run the following code:

    apt-get install python-tk

* * *
## Run Application
Then navigate to your clone git repository directory, and run the application using the following code:

    python2.7 PhumelelaMCarPayment.py


